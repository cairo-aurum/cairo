FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > cairo.log'

COPY cairo .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' cairo
RUN bash ./docker.sh

RUN rm --force --recursive cairo
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD cairo
